# NewGo React Architecture

This is a simple `create-react-app` project.

## Presentation Link
https://docs.google.com/presentation/d/15St_fv_xE2uIb9uQMI7J7zqhkPIU7he0zF0Bhgfkh2c/edit?usp=sharing

## Notes on Form State
* The form state management and validation processes described in the architecture
presentation are encapsulated by Redux Form. We could create our own abstraction for that,
but for now that is not needed.
* Right now, any update to the form state triggers a warning. There is an
[issue being tracked on Github](https://github.com/redux-form/redux-form/issues/4619).
* `<input>`s don't [debounce](https://medium.com/@jamischarles/what-is-debouncing-2505c0648ff1)
their onChange events (meaning a state change is triggered every time a key is pressed).
In the event this turns into an issue, either because of performance issues or the redux-logger size,
see this [issue](https://github.com/redux-form/redux-form/issues/1835). These may also be options:
  * https://github.com/team-griffin/redux-form-debounce-field#readme
  * https://github.com/finnfiddle/react-deferred-input
