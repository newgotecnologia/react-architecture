import BlockUi from "react-block-ui";
import React from "react";
import {connect} from "react-redux";

function UIBlocker({ blocking, children }) {
  return <BlockUi blocking={blocking}>
    {children}
  </BlockUi>
}

const mapStateToProps = state => ({
  blocking: state.blockUI,
});

export default connect(
  mapStateToProps
)(UIBlocker);
