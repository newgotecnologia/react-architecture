import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

function PrivateComponent({currentRoles = [], neededRoles = [], allowWithAny = true, renderIfAllowed, renderIfNotAllowed = () => <></>, children}) {
  if (children && renderIfAllowed) {
    throw new Error("Pass only renderIfAllowed OR children. Not both");
  }

  let hasRole;

  if (Array.isArray(neededRoles)) {
    if (allowWithAny) {
      hasRole = neededRoles.some(neededRole => currentRoles.includes(neededRole));
    } else {
      hasRole = neededRoles.every(neededRole => currentRoles.includes(neededRole));
    }
  } else if (typeof neededRoles === "string") {
    if ("*" === neededRoles) hasRole = true;
    hasRole = currentRoles.includes(neededRoles);
  }

  if (hasRole) {
    if (renderIfAllowed) {
      return renderIfAllowed();
    }
    return children;
  }

  return renderIfNotAllowed();
}

PrivateComponent.propTypes = {
  neededRoles: PropTypes.oneOf([
    PropTypes.arrayOf(PropTypes.string,),
    PropTypes.string,
  ]),

  allowWithAny: PropTypes.bool,

  renderIfAllowed: PropTypes.func,
  renderIfNotAllowed: PropTypes.func,
  children: PropTypes.node,
};

const mapStateToProps = (state) => ({
  currentRoles: state.auth.authenticated ? state.auth.user.permissions : []
});

export default connect(
  mapStateToProps
)(PrivateComponent);
