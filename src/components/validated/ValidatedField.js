import React, {useContext} from "react";
import {Field} from "redux-form";
import {ValidatedFormContext} from "./constants";
import {BusinessRulesContext} from "../../App";

const resolveRules = (prefix, name, businessRules, props) => {
  const validations = [];
  for (const [prop, value] of Object.entries(props)) {
    if (prop.startsWith(prefix)) {
      if (value) {
        const ruleName = prop.replace(prefix, "");

        // https://stackoverflow.com/a/33704783
        const capitalizedRuleName = ruleName[0].toLowerCase() + ruleName.slice(1);

        const rule = businessRules[capitalizedRuleName];

        if (!rule) {
          console.warn("No business rule named " + capitalizedRuleName + " found");
          continue;
        }

        validations.push(rule);
      }
    }
  }

  return validations;
};

function ValidatedField(props) {
  const businessRules = useContext(BusinessRulesContext);
  const validationState = useContext(ValidatedFormContext);

  const {name, ...rest} = props;

  validationState.registerValidations(name, resolveRules(validationState.validatePropPrefix, name, businessRules, rest));
  validationState.registerWarnings(name, resolveRules(validationState.warnPropPrefix, name, businessRules, rest));

  return <Field validationState={validationState} {...props} />;
}

export default ValidatedField;
