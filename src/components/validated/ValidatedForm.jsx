import React, {useContext} from "react";
import {reduxForm} from "redux-form";
import {ValidatedFormContext} from "./constants";

const getDisplayName = (Comp) => Comp.displayName || Comp.name || 'Component';

const run = (values, businessRules) => {
  const result = {};

  for (const [name, rules] of Object.entries(businessRules)) {
    if (rules) {
      for (const rule of rules) {
        const message = rule(values[name], values);
        if (message) {
          result[name] = message;
          break;
        }
      }
    }
  }

  return result
};

function validatedForm({validatePropPrefix = "val", warnPropPrefix = "warn"}) {
  const fieldValidations = {};
  const fieldWarnings = {};

  const validationState = {
    registerValidations: (name, validations) => {
      fieldValidations[name] = validations;
    },
    registerWarnings: (name, warnings) => {
      fieldWarnings[name] = warnings;
    },
    validatePropPrefix,
    warnPropPrefix,
  };

  const validate = (values) => run(values, fieldValidations);
  const warn = (values) => run(values, fieldWarnings);

  return (WrappedComponent) => {
    function GeneratedForm(props) {
      const {handleSubmit} = props;

      return <ValidatedFormContext.Provider value={validationState}>
        <form onSubmit={handleSubmit}>
          <WrappedComponent {...props} />
        </form>
      </ValidatedFormContext.Provider>;
    }

    GeneratedForm.displayName = getDisplayName(WrappedComponent);

    return reduxForm({
      form: GeneratedForm.displayName,
      validate,
      warn,
    })(GeneratedForm);
  }
}

export default validatedForm;
