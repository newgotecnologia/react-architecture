import React from "react";

export const REGISTER_SYMBOL = Symbol("FORM_VALIDATION_REGISTER_SYMBOL");
export const UNREGISTER_SYMBOL = Symbol("FORM_VALIDATION_UNREGISTER_SYMBOL");

export const ValidatedFormContext = React.createContext(null);
