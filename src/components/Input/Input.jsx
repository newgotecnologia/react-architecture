import React, {useState, useEffect} from 'react';
import PropTypes from "prop-types";

let currentInputId = 0;

function Input({label, type = "text", help, placeholder, value, onChange, disabled, postContent}) {
  const [id, setId] = useState(null);

  useEffect(() => {
    setId("input-" + (currentInputId++));
  }, []);

  const helpId = `${id}-help`;

  return <div className="form-group">
    {label && <label htmlFor={id}>{label}</label>}

    <input id={id} type={type} className="form-control"
           aria-describedby={helpId} placeholder={placeholder}
           value={value} onChange={onChange} disabled={disabled}/>

    {help && <small id={helpId} className="form-text text-muted">{help}</small>}
    {postContent}
  </div>;
}

Input.propTypes = {
  type: PropTypes.string,
  postContent: PropTypes.node,
};

export default Input;
