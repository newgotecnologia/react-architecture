import React from "react";
import PropTypes from "prop-types";
import {Redirect, Route} from "react-router-dom";
import PrivateComponent from "./PrivateComponent";

// Adapted from https://reacttraining.com/react-router/web/example/auth-workflow
function PrivateRoute(props) {
  const {children, component, neededRoles = [], ...rest} = props;
  return (
    <Route
      {...rest}
      render={({location}) => {
        return <PrivateComponent
          allowWithAny={true}
          neededRoles={neededRoles}
          renderIfAllowed={() => React.createElement(component, props)}
          renderIfNotAllowed={() =>
            <Redirect
              to={{
                pathname: "/login",
                state: {from: location}
              }}
            />
          }
        />;
      }}
    />
  );
}

PrivateRoute.propTypes = {
  neededRoles: PropTypes.oneOf([
    PropTypes.arrayOf(PropTypes.string,),
    PropTypes.string,
  ]),
};

export default PrivateRoute;
