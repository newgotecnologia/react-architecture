# The components/ folder

This folder contains reusable visual or connection components, each in its own folder.

Some of these examples would not exist in a real project.
[There are](https://www.npmjs.com/package/react-bootstrap)
[a lot of](https://www.npmjs.com/package/jqwidgets-scripts)
[React](https://www.npmjs.com/package/uikit-react)
[component](https://www.npmjs.com/package/uikit-react)
[NPM](https://www.npmjs.com/package/react-bulma-components)
[packages](https://www.npmjs.com/package/semantic-ui-react).
These **can** be used and that is encouraged. Components created here
can simply encapsulate one or more of these.

> Everything depends on your use case

 by the components in [React Bootstrap](https://react-bootstrap.github.io/).
These are created by me for demonstration purposes.

## Folder Structure
Note that not every component needs a folder. A `package.json`
file is imported the same way a single file is, so there is no
need to refactor other parts of the project if the component
structure is changed from a single file to a folder.

```
<Component>/
  <Component>.module.css
  <Component>.jsx
  package.json
```

**Note**  
`package.json` content will always be:

```
{
  "main": "<Component>.jsx"
}
```
