import React from "react";
import Input from "../Input";

import styles from "./FormInput.module.css";

function FormInput(props) {
  const {input, meta: {touched, error, warning}} = props;

  const postContent = touched &&
    ((error && <small className={styles.error}>{error}</small>) ||
      (warning && <small className={styles.warning}>{warning}</small>));

  return <Input {...props} {...input} postContent={postContent} />;
}

export default FormInput;
