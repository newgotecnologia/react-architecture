import React from "react";
import PrivateComponent from "../../components/PrivateComponent";

import styles from "./TodoPage.module.css";

function TodoPage() {

  return <div className={styles.page}>
    <div className={styles.container}>
      <h1>TODOs</h1>
      <ul>
        <PrivateComponent neededRoles={["VIEW_TODOS"]}>
          <li>you can view todos</li>
        </PrivateComponent>
        <PrivateComponent neededRoles={["VIEW_POKEMONS"]}>
          <li>you can view POKEMONS!!</li>
        </PrivateComponent>
      </ul>
    </div>
  </div>;
}

export default TodoPage;
