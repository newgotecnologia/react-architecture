import React from "react";
import Input from "../../../../components/Input";
import FormInput from "../../../../components/FormInput";
import ValidatedForm from "../../../../components/validated/ValidatedForm";
import ValidatedField from "../../../../components/validated/ValidatedField";

/**
 * This is a Connection Component for creating the Login Form.
 * It integrates with Redux Form to connect the state to the form, passing
 * the field values
 */

function LoginForm({ submitting }) {
  return <>
    <ValidatedField valRequired valIsEmail valNPodeSerOi
                    name="email" component={FormInput}
                    label="Login" placeholder="seujoao@email.com"
                    help="(dica: digite 'ioxua@newgo.com')"
    />

    <ValidatedField valRequired
                    name="passwd" component={FormInput}
                    label="Senha" placeholder="gatinho123" type="password"
                    help="(dica: digite 'batata22')"
    />

    <Input type="submit" value="Entrar!" disabled={submitting}/>
  </>
}

export default ValidatedForm({})(LoginForm)
