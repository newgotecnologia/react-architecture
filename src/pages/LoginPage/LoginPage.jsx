import React, {useState} from 'react';

import styles from "./LoginPage.module.css";
import LoginForm from "./components/LoginForm";
import store from "../../store";
import {tryLogin} from "../../store/actions";

function LoginPage(props) {
  const [error, setError] = useState(null);
  const onLogin = (values) => {
    store.dispatch(tryLogin(values.email, values.passwd))
      .then(() => props.history.push(`/todos`))
      .catch((e) => setError(e));
  };

  return <div className={styles.page}>
    <div className={styles.formWrapper}>
      {error && <div className="alert alert-danger" role="alert">
        {error}
      </div>}
      <LoginForm onSubmit={onLogin}/>
    </div>
  </div>;
}

export default LoginPage;
