
export const businessRules = {
  required: (value) => {
    if (!value) {
      return "O campo é obrigatório";
    }
  },
  isEmail: (value) => {
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      return "Digite um email válido";
    }
  },
  nPodeSerOi: (value, allValues) => {
    if (value === "oi@ioxua.com") {
      return "Tem certeza de que deve usar esse email?"
    }
  }
};
