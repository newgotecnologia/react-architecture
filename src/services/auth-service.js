
export const tryToLogin = (login, passwd) => new Promise((resolve, reject) => {
  // Simulate a request :)
  setTimeout(() => {
    if ("ioxua@newgo.com" === login && "batata22" === passwd) {
      resolve({
        name: "Yehoshua Oliveira",
        permissions: [
          // Try uncommenting this line
          // "VIEW_POKEMONS",
          "VIEW_TODOS",
          "CREATE_TODO"
        ]
      })
    } else {
      reject("Email ou senha errados");
    }
  }, 1500);
});
