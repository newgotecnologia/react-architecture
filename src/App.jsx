import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import LoginPage from './pages/LoginPage';
import {Provider} from "react-redux";
import store from "./store";
import PrivateRoute from "./components/PrivateRoute";
import TodoPage from "./pages/TodoPage";
import {businessRules} from "./services/business-rules";
import UIBlocker from "./components/UIBlocker";

// Here we import Bootstrap as a global CSS file
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import 'react-block-ui/style.css';

export const BusinessRulesContext = React.createContext(null);

function App() {
  return <BusinessRulesContext.Provider value={businessRules}>
    <Provider store={store}>
      <UIBlocker>
        {/* This router can be added to a separate file later in the project */}
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={LoginPage}/>
            <Route exact path="/login" component={LoginPage}/>
            <PrivateRoute path="/todos" component={TodoPage} neededRoles={["VIEW_TODOS"]}/>
          </Switch>
        </BrowserRouter>
      </UIBlocker>
    </Provider>
  </BusinessRulesContext.Provider>;
}

export default App;
