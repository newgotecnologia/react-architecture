import {
  LOGIN_ERROR,
  LOGOFF,
  LOGIN_SUCCESS,
  BLOCK_UI,
  UNBLOCK_UI
} from "./actions";
import {reducer as formReducer} from "redux-form";
import {combineReducers} from "redux";

// The first argument is the current state, before the actions is executed.
// For now this is an empty object, but in the future this may come from
// a service
function authReducer(state = {}, action) {
  switch (action.type) {
    case LOGIN_SUCCESS: return {
      ...state,
      authenticated: true,
      user: action.payload
    };
    case LOGIN_ERROR: return {
      ...state,
      authenticated: false,
      error: action.error,
    };
    case LOGOFF: return {
      ...state,
      authenticated: false,
      error: null,
      user: null,
    };
    default: return state;
  }
}

function blockReducer(state = false, action) {
  switch (action.type) {
    case BLOCK_UI: return true;
    case UNBLOCK_UI: return false;
    default: return state;
  }
}

export const rootReducer = combineReducers({
  form: formReducer,
  auth: authReducer,
  blockUI: blockReducer,
});

export default rootReducer;
