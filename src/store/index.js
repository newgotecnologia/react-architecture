import {createStore, applyMiddleware} from "redux";
import rootReducer from "./reducers";
import logger from "redux-logger";
import thunk from "redux-thunk";

const buildStore = () => createStore(
  rootReducer,
  applyMiddleware(logger, thunk)
);

const store = buildStore();

export default store;
