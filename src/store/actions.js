import {tryToLogin} from "../services/auth-service";

const PREFIX = "@project/";

export const BLOCK_UI = `${PREFIX}BLOCK_UI`;
export const UNBLOCK_UI = `${PREFIX}UNBLOCK_UI`;

export const TRY_LOGIN = `${PREFIX}TRY_LOGIN`;
export const LOGIN_SUCCESS = `${PREFIX}LOGIN_SUCCESS`;
export const LOGIN_ERROR = `${PREFIX}LOGIN_ERROR`;
export const LOGOFF = `${PREFIX}LOGOFF`;

// redux-thunk allows us to create async action creators, effectively making
// actions that can dispatch other actions

export const tryLogin = (login, passwd) => (dispatch/*, getState*/) => {
  dispatch(blockUI());
  return tryToLogin(login, passwd)
    .then(user => dispatch(loginSuccess(user)))
    .catch(e => {
      dispatch(loginError(e));
      throw e;
    })
    .finally(() => dispatch(unblockUI()));
};

export const loginSuccess = (user) => ({type: LOGIN_SUCCESS, payload: user});
export const loginError = (error) => ({type: LOGIN_ERROR, error});
export const logOff = () => ({type: LOGOFF});
export const blockUI = () => ({ type: BLOCK_UI });
export const unblockUI = () => ({ type: UNBLOCK_UI });
